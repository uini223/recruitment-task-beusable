package com.ebartnik.recruitmenttask.occupancycalculator.core;

import java.util.List;

record RoomPlacingContext(
        RoomsAvailability roomsAvailability,
        List<PotentialGuest> potentialGuests,
        RoomOccupancy roomOccupancy
) {

     RoomPlacingContext(RoomsAvailability roomsAvailability, List<PotentialGuest> potentialGuests) {
        this(roomsAvailability, potentialGuests, RoomOccupancy.empty());
    }

    int noOfAvailableRooms(RoomType roomType) {
        return roomsAvailability.getNoOfAvailableRooms(roomType);
    }

    boolean areMoreRoomsAvailable(RoomType roomType) {
        return noOfAvailableRoomsLeft(roomType) > 0;
    }

    int noOfAvailableRoomsLeft(RoomType roomType) {
        return noOfAvailableRooms(roomType) - roomOccupancy.noOfOccupiedRooms(roomType);
    }

    void placeGuestInARoom(RoomType roomType, PotentialGuest potentialGuest) {
        roomOccupancy.addGuest(roomType, potentialGuest);
    }
}
