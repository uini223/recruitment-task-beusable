package com.ebartnik.recruitmenttask.occupancycalculator.core;

import java.util.List;

public interface PotentialGuestsProvider {

    List<PotentialGuest> get();
}
