package com.ebartnik.recruitmenttask.occupancycalculator.core;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class OccupancyCalculatorCoreBeansConfiguration {

    @Bean
    PriceBasedRoomTypeClassifier priceBasedRoomTypeClassifier() {
        return new PriceBasedRoomTypeClassifier(Price.of(100));
    }

    @Bean
    PremiumRoomPlacingStrategy premiumRoomPlacingStrategy(RoomTypeClassifier roomTypeClassifier) {
        return new PremiumRoomPlacingStrategy(roomTypeClassifier);
    }

    @Bean
    StandardRoomPlacingStrategy standardRoomPlacingStrategy(RoomTypeClassifier roomTypeClassifier) {
        return new StandardRoomPlacingStrategy(roomTypeClassifier);
    }

    @Bean
    RoomOccupancyCalculator roomOccupancyCalculator(StandardRoomPlacingStrategy standardRoomPlacingStrategy,
                                                    PremiumRoomPlacingStrategy premiumRoomPlacingStrategy) {
        return new RoomOccupancyCalculator(
                List.of(premiumRoomPlacingStrategy, standardRoomPlacingStrategy)
        );
    }


}
