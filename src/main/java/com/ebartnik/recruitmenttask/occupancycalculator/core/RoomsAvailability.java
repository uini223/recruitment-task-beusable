package com.ebartnik.recruitmenttask.occupancycalculator.core;

import lombok.Builder;
import lombok.NonNull;
import lombok.Singular;

import java.util.Map;

@Builder
public record RoomsAvailability(
        @NonNull @Singular("availability") Map<RoomType, Integer> availability
) {

    int getNoOfAvailableRooms(RoomType roomType) {
        return availability.getOrDefault(roomType, 0);
    }
}
