package com.ebartnik.recruitmenttask.occupancycalculator.core;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class PriceBasedRoomTypeClassifier implements RoomTypeClassifier {

    private final Price thresholdPrice;

    @Override
    public RoomType classify(PotentialGuest potentialGuest) {
        if (potentialGuest.priceCustomerIsWillingToPay().isGreaterThanOrEqualTo(thresholdPrice)) {
            return RoomType.PREMIUM;
        }
        return RoomType.STANDARD;
    }
}
