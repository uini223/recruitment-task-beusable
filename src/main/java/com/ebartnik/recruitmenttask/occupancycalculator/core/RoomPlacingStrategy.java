package com.ebartnik.recruitmenttask.occupancycalculator.core;

interface RoomPlacingStrategy {

    void placeGuests(RoomPlacingContext roomPlacingContext);
}
