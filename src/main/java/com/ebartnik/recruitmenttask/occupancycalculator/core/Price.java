package com.ebartnik.recruitmenttask.occupancycalculator.core;

/**
 * Class representing positive value of a price.
 * This restriction is given only based on task description, in real world this class could be used in other class
 * representing e.g. discount. In such case Money archetype pattern should to keep flexibility.
 * @param value must be value greater than 0
 */
public record Price(double value) implements Comparable<Price> {
    public Price {
        if (value < 0) {
            throw new IllegalArgumentException("Price cannot take negative value");
        }
    }

    @Override
    public int compareTo(Price o) {
        return Double.compare(value, o.value);
    }

    public boolean isGreaterThanOrEqualTo(Price other) {
        return this.value >= other.value;
    }

    public Price add(Price price) {
        return new Price(value + price.value);
    }

    public static Price zero() {
        return new Price(0);
    }

    public static Price of(double v) {
        return new Price(v);
    }
}
