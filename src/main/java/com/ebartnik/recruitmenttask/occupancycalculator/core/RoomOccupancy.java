package com.ebartnik.recruitmenttask.occupancycalculator.core;

import lombok.Getter;
import lombok.NonNull;

import java.util.HashMap;
import java.util.Map;

public final class RoomOccupancy {
    @Getter
    private final Map<RoomType, RoomsUsage> occupancy;

    private RoomOccupancy(@NonNull Map<RoomType, RoomsUsage> occupancy) {
        this.occupancy = occupancy;
    }

    private RoomOccupancy() {
        this(new HashMap<>());
    }

    public static RoomOccupancy empty() {
        return new RoomOccupancy();
    }

    void addGuest(RoomType roomType, PotentialGuest potentialGuest) {
        RoomsUsage roomsUsage = getRoomsUsage(roomType);
        roomsUsage.addGuest(potentialGuest);
    }

    public int noOfOccupiedRooms(RoomType roomType) {
        RoomsUsage roomsUsage = getRoomsUsage(roomType);
        return roomsUsage.getNoOfRoomsOccupied();
    }

    public Price totalPrice(RoomType roomType) {
        RoomsUsage roomsUsage = getRoomsUsage(roomType);
        return roomsUsage.getTotalPrice();
    }

    private RoomsUsage getRoomsUsage(RoomType roomType) {
        return occupancy.computeIfAbsent(roomType, rt -> RoomsUsage.empty());
    }
}
