package com.ebartnik.recruitmenttask.occupancycalculator.core;

import lombok.NonNull;

public record PotentialGuest(@NonNull Price priceCustomerIsWillingToPay) implements Comparable<PotentialGuest> {

    @Override
    public int compareTo(PotentialGuest o) {
        return this.priceCustomerIsWillingToPay.compareTo(o.priceCustomerIsWillingToPay);
    }

}
