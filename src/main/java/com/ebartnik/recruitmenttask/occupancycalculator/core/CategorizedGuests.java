package com.ebartnik.recruitmenttask.occupancycalculator.core;

import lombok.NonNull;

import java.util.Collections;
import java.util.List;
import java.util.Map;

record CategorizedGuests(
        @NonNull Map<RoomType, List<PotentialGuest>> categorizedGuests
) {

    int getNumberOfUsersCategorizedToRoomType(RoomType roomType) {
        return getGuestsFor(roomType).size();
    }

    List<PotentialGuest> getGuestsFor(RoomType roomType) {
        return categorizedGuests.getOrDefault(roomType, Collections.emptyList());
    }
}
