package com.ebartnik.recruitmenttask.occupancycalculator.core;

interface RoomTypeClassifier {
    RoomType classify(PotentialGuest potentialGuest);
}
