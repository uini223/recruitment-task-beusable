package com.ebartnik.recruitmenttask.occupancycalculator.core;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

@Getter
@EqualsAndHashCode
@ToString
public final class RoomsUsage {
    private int noOfRoomsOccupied;
    private Price totalPrice;


    public RoomsUsage(int noOfRoomsOccupied, @NonNull Price totalPrice) {
        if (noOfRoomsOccupied < 0) {
            throw new RuntimeException("Minimum number of occupied rooms is 0");
        }
        this.noOfRoomsOccupied = noOfRoomsOccupied;
        this.totalPrice = totalPrice;
    }

    public static RoomsUsage empty() {
        return new RoomsUsage(0, Price.zero());
    }

    public void addGuest(PotentialGuest potentialGuest) {
        noOfRoomsOccupied++;
        totalPrice = totalPrice.add(potentialGuest.priceCustomerIsWillingToPay());
    }

}
