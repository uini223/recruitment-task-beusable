package com.ebartnik.recruitmenttask.occupancycalculator.core;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Comparator;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
class StandardRoomPlacingStrategy implements RoomPlacingStrategy {

    private final RoomTypeClassifier roomTypeClassifier;

    @Override
    public void placeGuests(RoomPlacingContext roomPlacingContext) {
        List<PotentialGuest> guestsClassifiedForStandardRoom = roomPlacingContext.potentialGuests()
                .stream()
                .filter(guest -> RoomType.STANDARD.equals(roomTypeClassifier.classify(guest)))
                .sorted(Comparator.reverseOrder())
                .toList();

        boolean guestsCanBeUpgraded = guestCanBeUpgraded(roomPlacingContext, guestsClassifiedForStandardRoom);

        for (PotentialGuest potentialGuest : guestsClassifiedForStandardRoom) {
            if (guestsCanBeUpgraded && thereAreAvailableRoomsForUpgrade(roomPlacingContext)) {
                log.debug("There are still premium rooms available, adding standard customer %s".formatted(potentialGuest));
                roomPlacingContext.placeGuestInARoom(RoomType.PREMIUM, potentialGuest);
            } else if (roomPlacingContext.areMoreRoomsAvailable(RoomType.STANDARD)) {
                log.debug("There are still standard rooms available, adding %s".formatted(potentialGuest));
                roomPlacingContext.placeGuestInARoom(RoomType.STANDARD, potentialGuest);
            } else {
                break;
            }
        }
    }

    private boolean guestCanBeUpgraded(RoomPlacingContext roomPlacingContext,
                                       List<PotentialGuest> guestsClassifiedForStandardRoom) {
        return thereAreAvailableRoomsForUpgrade(roomPlacingContext) &&
                roomPlacingContext.noOfAvailableRooms(RoomType.STANDARD) < guestsClassifiedForStandardRoom.size();
    }

    private boolean thereAreAvailableRoomsForUpgrade(RoomPlacingContext roomPlacingContext) {
        return roomPlacingContext.noOfAvailableRoomsLeft(RoomType.PREMIUM) > 0;
    }
}
