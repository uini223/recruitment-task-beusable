package com.ebartnik.recruitmenttask.occupancycalculator.core;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class RoomOccupancyCalculator {

    private final List<RoomPlacingStrategy> roomPlacingStrategies;


    public RoomOccupancy calculate(List<PotentialGuest> potentialGuests,
                                   RoomsAvailability roomsAvailability) {
        if (potentialGuests == null || potentialGuests.isEmpty()) {
            return RoomOccupancy.empty();
        }

        RoomPlacingContext roomPlacingContext = new RoomPlacingContext(roomsAvailability, potentialGuests);
        roomPlacingStrategies.forEach(roomPlacingStrategy -> roomPlacingStrategy.placeGuests(roomPlacingContext));

        return roomPlacingContext.roomOccupancy();
    }

}
