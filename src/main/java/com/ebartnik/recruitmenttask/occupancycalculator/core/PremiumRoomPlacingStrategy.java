package com.ebartnik.recruitmenttask.occupancycalculator.core;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Comparator;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
class PremiumRoomPlacingStrategy implements RoomPlacingStrategy {

    private final RoomTypeClassifier roomTypeClassifier;

    @Override
    public void placeGuests(RoomPlacingContext roomPlacingContext) {
        List<PotentialGuest> potentialGuestsClassifiedForPremiumRoom = roomPlacingContext.potentialGuests()
                .stream()
                .filter(guest -> RoomType.PREMIUM.equals(roomTypeClassifier.classify(guest)))
                .sorted(Comparator.reverseOrder())
                .toList();

        for (PotentialGuest potentialPremiumGuest : potentialGuestsClassifiedForPremiumRoom) {
            if (roomPlacingContext.areMoreRoomsAvailable(RoomType.PREMIUM)) {
                log.debug("There are still premium rooms available, adding %s".formatted(potentialPremiumGuest));
                roomPlacingContext.placeGuestInARoom(RoomType.PREMIUM, potentialPremiumGuest);
            }
        }
    }
}
