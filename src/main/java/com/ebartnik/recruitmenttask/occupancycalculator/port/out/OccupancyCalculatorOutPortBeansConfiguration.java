package com.ebartnik.recruitmenttask.occupancycalculator.port.out;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OccupancyCalculatorOutPortBeansConfiguration {

    @Bean
    InMemoryPotentialGuestsProvider inMemoryPotentialGuestsProvider() {
        return new InMemoryPotentialGuestsProvider();
    }
}
