package com.ebartnik.recruitmenttask.occupancycalculator.port.out;

import com.ebartnik.recruitmenttask.occupancycalculator.core.PotentialGuest;
import com.ebartnik.recruitmenttask.occupancycalculator.core.PotentialGuestsProvider;
import com.ebartnik.recruitmenttask.occupancycalculator.core.Price;

import java.util.List;
import java.util.stream.DoubleStream;

public class InMemoryPotentialGuestsProvider implements PotentialGuestsProvider {
    private static final List<PotentialGuest> POTENTIAL_GUESTS = DoubleStream.of(
                    23, 45, 155, 374, 22, 99.99, 100, 101, 115, 209
            ).mapToObj(number -> new PotentialGuest(Price.of(number)))
            .toList();

    @Override
    public List<PotentialGuest> get() {
        return POTENTIAL_GUESTS;
    }
}
