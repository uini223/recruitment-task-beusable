package com.ebartnik.recruitmenttask.occupancycalculator.port.in;


import com.ebartnik.recruitmenttask.occupancycalculator.core.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rooms/occupancy")
public class RoomOccupancyCalculatorController {

    public static final String PRICE_DISPLAY_PATTERN = "EUR %s";

    private final RoomOccupancyCalculator roomOccupancyCalculator;
    private final PotentialGuestsProvider potentialGuestsProvider;

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public RoomsOccupancyDto calculateRoomsOccupancy(
            @RequestParam(value = "numberOfFreeStandardRooms", defaultValue = "0") int noOfAvailableStandardRooms,
            @RequestParam(value = "numberOfFreePremiumRooms", defaultValue = "0") int noOfAvailablePremiumRooms
    ) {
        List<PotentialGuest> potentialGuests = potentialGuestsProvider.get();

        RoomsAvailability roomsAvailability = RoomsAvailability.builder()
                .availability(RoomType.STANDARD, noOfAvailableStandardRooms)
                .availability(RoomType.PREMIUM, noOfAvailablePremiumRooms)
                .build();

        RoomOccupancy calculatedOccupancy = roomOccupancyCalculator.calculate(
                potentialGuests,
                roomsAvailability
        );

        return new RoomsOccupancyDto(
                calculatedOccupancy.noOfOccupiedRooms(RoomType.PREMIUM),
                PRICE_DISPLAY_PATTERN.formatted(calculatedOccupancy.totalPrice(RoomType.PREMIUM).value()),
                calculatedOccupancy.noOfOccupiedRooms(RoomType.STANDARD),
                PRICE_DISPLAY_PATTERN.formatted(calculatedOccupancy.totalPrice(RoomType.STANDARD).value())
        );
    }

    @Data
    @AllArgsConstructor
    private static class RoomsOccupancyDto {
        private int usagePremium;
        private String totalPriceForPremium;

        private int usageStandard;
        private String totalPriceForStandard;
    }
}
