package com.ebartnik.recruitmenttask.occupancycalculator.core;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class RoomOccupancyUnitTest {

    @Test
    void shouldCorrectlyAddGuestsAndCalculateTotalForEachType() {
        // given
        RoomOccupancy sut = RoomOccupancy.empty();

        // when
        sut.addGuest(RoomType.STANDARD, new PotentialGuest(Price.of(10)));
        sut.addGuest(RoomType.STANDARD, new PotentialGuest(Price.of(11)));
        sut.addGuest(RoomType.STANDARD, new PotentialGuest(Price.of(12)));

        sut.addGuest(RoomType.PREMIUM, new PotentialGuest(Price.of(23)));
        sut.addGuest(RoomType.PREMIUM, new PotentialGuest(Price.of(31)));

        // then
        assertThat(sut.noOfOccupiedRooms(RoomType.STANDARD))
                .isEqualTo(3);
        assertThat(sut.getOccupancy().get(RoomType.STANDARD))
                .extracting(RoomsUsage::getTotalPrice)
                .isEqualTo(Price.of(33));

        assertThat(sut.noOfOccupiedRooms(RoomType.PREMIUM))
                .isEqualTo(2);
        assertThat(sut.getOccupancy().get(RoomType.PREMIUM))
                .extracting(RoomsUsage::getTotalPrice)
                .isEqualTo(Price.of(54));
    }
}