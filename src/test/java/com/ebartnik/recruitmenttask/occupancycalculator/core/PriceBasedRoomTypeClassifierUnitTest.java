package com.ebartnik.recruitmenttask.occupancycalculator.core;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class PriceBasedRoomTypeClassifierUnitTest {

    private static Stream<Arguments> shouldClassifyGuestsBasedOnThreshold() {
        return Stream.of(
                arguments(Price.of(100), new PotentialGuest(Price.of(99.99)), RoomType.STANDARD),
                arguments(Price.of(100), new PotentialGuest(Price.of(1)), RoomType.STANDARD),
                arguments(Price.of(99.99), new PotentialGuest(Price.of(99.99)), RoomType.PREMIUM),
                arguments(Price.of(99.99), new PotentialGuest(Price.of(100)), RoomType.PREMIUM),
                arguments(Price.of(99.99), new PotentialGuest(Price.of(99)), RoomType.STANDARD),
                arguments(Price.of(2), new PotentialGuest(Price.of(1)), RoomType.STANDARD)
        );
    }

    @ParameterizedTest
    @MethodSource("shouldClassifyGuestsBasedOnThreshold")
    void shouldClassifyGuestsBasedOnThreshold(Price threshold, PotentialGuest potentialGuest,
                                              RoomType expectedRoomType) {
        PriceBasedRoomTypeClassifier sut = new PriceBasedRoomTypeClassifier(threshold);

        assertThat(sut.classify(potentialGuest))
                .isEqualTo(expectedRoomType);
    }
}