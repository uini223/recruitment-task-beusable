package com.ebartnik.recruitmenttask.occupancycalculator.core;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

@SpringBootTest
public class RoomOccupancyCalculatorIntegrationTest {

    private static final List<PotentialGuest> POTENTIAL_GUESTS = DoubleStream.of(
                    23, 45, 155, 374, 22, 99.99, 100, 101, 115, 209
            ).mapToObj(number -> new PotentialGuest(Price.of(number)))
            .toList();

    @Autowired
    private RoomOccupancyCalculator sut;

    static Stream<Arguments> availableRoomsWithExpectedOccupancy() {
        return Stream.of(
                arguments("Should fill all rooms with top paying guests if number of available room is lower than number of potential guests",
                        3, 3, new RoomsUsage(3, Price.of(738)), new RoomsUsage(3, Price.of(167.99))),
                arguments("Should fill all rooms when number of available rooms is higher than number of potential guests",
                        7, 5, new RoomsUsage(6, Price.of(1054)), new RoomsUsage(4, Price.of(189.99))),
                arguments("Should fill premium rooms with top paying customers and fill all standard rooms with customers not considered premium",
                        2, 7, new RoomsUsage(2, Price.of(583)), new RoomsUsage(4, Price.of(189.99))),
                arguments("Should fill all premium rooms with top paying customers when there is no more place in standard rooms",
                        7, 1, new RoomsUsage(7, Price.of(1153.99)), new RoomsUsage(1, Price.of(45))),
                arguments("Should fill no rooms when there are no available",
                        0, 0, new RoomsUsage(0, Price.zero()), new RoomsUsage(0, Price.zero()))
        );
    }

    @ParameterizedTest(name = "{index}: {0}")
    @MethodSource("availableRoomsWithExpectedOccupancy")
    void shouldCalculateNumberOfOccupiedRoomsAndExpectedPriceCorrectly(
            String name,
            int noOfPremiumRoomsAvailable,
            int noOfStandardRoomsAvailable,
            RoomsUsage expectedPremiumRoomsUsage,
            RoomsUsage expectedStandardRoomsUsage) {

        // given
        RoomsAvailability roomsAvailability = new RoomsAvailability(Map.of(
                RoomType.PREMIUM, noOfPremiumRoomsAvailable,
                RoomType.STANDARD, noOfStandardRoomsAvailable));

        // when

        RoomOccupancy roomOccupancy = sut.calculate(POTENTIAL_GUESTS, roomsAvailability);


        // then
        assertThat(roomOccupancy.getOccupancy())
                .containsAllEntriesOf(
                        Map.of(
                                RoomType.PREMIUM, expectedPremiumRoomsUsage,
                                RoomType.STANDARD, expectedStandardRoomsUsage
                        )
                );
    }
}
