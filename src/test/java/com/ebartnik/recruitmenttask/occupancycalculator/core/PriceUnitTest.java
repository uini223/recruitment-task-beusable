package com.ebartnik.recruitmenttask.occupancycalculator.core;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class PriceUnitTest {

    @ParameterizedTest
    @ValueSource(doubles = {-200_000_000, -100, -3, -1})
    void shouldThrowExceptionWhenTryingToCreatePriceWithNegativeValue(double value) {
        assertThatThrownBy(() -> Price.of(value))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @ParameterizedTest
    @ValueSource(doubles = {0, 1, 3, 100, 200_000_000})
    void shouldCorrectlyAssignValueWhenIsGreaterOrEqualToZero(double value) {
        assertThat(Price.of(value))
                .extracting(Price::value)
                .isEqualTo(value);
    }

    @Test
    void shouldReturnPriceWithValueZero() {
        assertThat(Price.zero())
                .extracting(Price::value)
                .isEqualTo(0d);
    }

    private static Stream<Arguments> shouldCorrectlyCheckIfIsGreaterOrEqualTo() {
        return Stream.of(
                arguments(Price.of(0), Price.of(0), true),
                arguments(Price.of(1), Price.of(0), true),
                arguments(Price.of(0), Price.of(1), false),
                arguments(Price.of(1_999_999), Price.of(2_000_000), false)
        );
    }

    @ParameterizedTest
    @MethodSource("shouldCorrectlyCheckIfIsGreaterOrEqualTo")
    void shouldCorrectlyCheckIfIsGreaterOrEqualTo(Price p1, Price p2, boolean expectedResult) {
        assertThat(p1.isGreaterThanOrEqualTo(p2))
                .isEqualTo(expectedResult);
    }


    private static Stream<Arguments> shouldCorrectlyAddPrice() {
        return Stream.of(
                arguments(Price.of(0), Price.of(0), Price.zero()),
                arguments(Price.of(1), Price.of(0), Price.of(1)),
                arguments(Price.of(0.99), Price.of(1.89), Price.of(2.88)),
                arguments(Price.of(1_999_999.33), Price.of(2_000_000.66), Price.of(3_999_999.99))
        );
    }

    @ParameterizedTest
    @MethodSource("shouldCorrectlyAddPrice")
    void shouldCorrectlyAddPrice(Price p1, Price p2, Price expectedResult) {
        assertThat(p1.add(p2))
                .isEqualTo(expectedResult);
    }
}