# Recruitment task

## Note
I must say I was enjoying simple yet well-formed task.
You can see how code evolved since first commit till the last. 
I use my Tests as a base for refactoring where I could be sure that after changes logic stays the same for the cases.
While creating the solution I was focused on:
* Readability
* Maintainability
* Extensibility

As for the first point I created a lot of Value Objects for representing some concepts e.g. Price. 
Using Value Object here can be beneficial as you can swap the type that stores value under the hood without impacting the rest.
e.g. from double to float or BigDecimal (this might be more tricky)

Value Objects can increase memory footprint, but I did not consider it in this task.
Keeping and updating the result in the assumes that guests for Standard Rooms are place after guests for Premium Rooms were places
(as we need to know if there are some available)

As for extensibility it should be easy to add different source of potential customers, or add API to manage them.
It should also be easy to change threshold value when placing customers as well as condition when guest can be 
upgraded to premium.

What could be improved more:
* Traceability and Logging
* Metrics
* Error Handling


## How it's solved
Solution is provided as a web service with REST API that returns the calculations based on given input
For now only acceptable parameters are:
* Number of Free Premium Rooms
* Number of Free Standard Rooms

List of potential guests is hardcoded as there was not requirement for any editing.

## How to run?
### Requirements
* Commands must be run in root project directory
* JAVA_HOME must be set to Java 17 location

### Tests
#### Bash
```bash
./gradlew test
```

#### Windows
```
gradlew.bat test
```

### Server start

#### Bash
```bash
./gradlew bootRun
```

#### Windows
```
gradlew.bat bootRun
```

Above commands starts the server that listens on port 8080

### API Documentation
There is swagger-ui available under:
```url
http://localhost:8080/swagger-ui/index.html
```
and also generated OpenAPI specification under url 
```url
http://localhost:8080/documentation
```

### How to get results?
After successfully running server, you can get the result by running HTTP GET request  
All responses are in JSON format

You can use swagger-ui to send a request or use curl from the console:

#### Test case 1
```bash
curl -v 'http://localhost:8080/rooms/occupancy?numberOfFreeStandardRooms=3&numberOfFreePremiumRooms=3'
```

#### Test case 2
```bash
curl -v 'http://localhost:8080/rooms/occupancy?numberOfFreeStandardRooms=5&numberOfFreePremiumRooms=7'
```

#### Test case 3
```bash
curl -v 'http://localhost:8080/rooms/occupancy?numberOfFreeStandardRooms=7&numberOfFreePremiumRooms=2'
```

#### Test case 4
```bash
curl -v 'http://localhost:8080/rooms/occupancy?numberOfFreeStandardRooms=1&numberOfFreePremiumRooms=7'
```

